#!/usr/bin/env sh

export POSTGRES_URL=localhost
export POSTGRES_PORT=5432
export POSTGRES_HOST=$POSTGRES_URL 
export POSTGRES_DB=postgres
export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=postgres
export COMMAND="ls -1 | grep sql$ | xargs -t -I % psql -h \"$POSTGRES_HOST\" -U \"$POSTGRES_USER\" -d \"$POSTGRES_DB\"  -f %"

echo $POSTGRES_DB
docker run --name db-postgres -p $POSTGRES_PORT:$POSTGRES_PORT -d postgres:alpine
sleep 1
ls -1 *.sql | xargs -I % docker cp % db-postgres:/
docker exec -it db-postgres /bin/sh -c "$COMMAND"
docker stop db-postgres
docker rm db-postgres
