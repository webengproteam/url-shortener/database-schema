-- Insert users
INSERT INTO users (email, passwd_hash) VALUES
('john@mail.com', '96D9632F363564CC3032521409CF22A852F2032EEC099ED5967C0D000CEC607A'),
('mary@mail.com', '6915771BE1C5AA0C886870B6951B03D7EAFC121FEA0E80A5EA83BEB7C449F4EC');

-- Insert URI
INSERT INTO uri (short_uri, long_uri, active, accessible, secure, admin) VALUES
('abc123', 'https://twitter.com', TRUE, FALSE, FALSE, 1),
('abc4', 'https://twitter.com', FALSE, FALSE, FALSE, 2),
('abc5', 'https://twitter.com', TRUE, TRUE, TRUE, 2),
('abc6', 'https://twitter.com', TRUE, FALSE, FALSE, 2),
('abc7', 'https://twitter.com', TRUE, TRUE, FALSE, 2)
;

-- Insert Access logs
INSERT INTO uri_access (datetime, browser, os, ip, uri) VALUES
('2019-11-05 09:15:23', 'Firefox', 'GNU/Linux', '38a0ddaad60365a349c255b9ad6e2ac2', 'abc123'),
('2019-11-05 09:20:25', 'Chrome', 'GNU/Linux', 'f81222c98c45c459e9befa4a67fcc371', 'abc123'),
('2019-11-05 09:22:33', 'Safari', 'MacOS', '820d3cd6cbbb3432b660741d2e54d104', 'abc123'),
('2019-11-05 09:30:45', 'Chrome', 'windows', 'a56bdbb2cc577c3eb01b8575c9241d51', 'abc123'),
('2019-11-05 10:15:23', 'Firefox', 'GNU/Linux', '38a0ddaad60365a349c255b9ad6e2ac2', 'abc123');

-- Insert Hooks
INSERT INTO hook_trigger (clicks, minutes, admin, uri, active) VALUES
(5, 120, 1, 'abc123', TRUE);

-- Insert Hook HTTP request actions
INSERT INTO hook_action_http (http_endp, http_verb, http_params, http_body, hook_id) VALUES
('http://myendpoint.org/here', 'GET', '?query=lol', '{"thing": {"hi": 3}}', 1);

-- Insert Hook Email actions
INSERT INTO hook_action_email (address, subject, body, hook_id) VALUES
('myfriend@mail.com', 'Hey!','Lorem ipsum', 1);

-- Insert Hook activation log
INSERT INTO hook_activation (datetime, clicks, minutes, hook_id) VALUES
('2019-11-05 10:16:04', 5, 60, 1);
