CREATE TABLE users (
    id          serial CONSTRAINT user_key PRIMARY KEY,
    email       text,
    passwd_hash text NOT NULL
);

CREATE TABLE uri (
    short_uri   varchar(20) CONSTRAINT uri_key PRIMARY KEY,
    long_uri    varchar(100) NOT NULL,
    accessible  boolean NOT NULL DEFAULT FALSE,
    active      boolean NOT NULL,
    secure      boolean NOT NULL DEFAULT FALSE,
    admin       serial REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE uri_access (
    id          serial CONSTRAINT uri_access_key PRIMARY KEY,
    datetime    timestamp NOT NULL,
    browser     text,
    os          text,
    ip          text,
    uri         varchar(20) REFERENCES uri(short_uri) ON DELETE CASCADE
);

CREATE TABLE hook_trigger (
    id          serial CONSTRAINT hook_key PRIMARY KEY,
    clicks      integer,
    minutes     integer,
    admin       serial REFERENCES Users(id) ON DELETE CASCADE,
    uri         varchar(20) REFERENCES uri(short_uri) ON DELETE CASCADE,
    active      boolean NOT NULL
);

CREATE TABLE hook_action_http (
    id          serial CONSTRAINT http_req_key PRIMARY KEY,
    http_endp   text,
    http_verb   text,
    http_params text,
    http_body   text,
    hook_id     serial REFERENCES hook_trigger(id) ON DELETE CASCADE
);

CREATE TABLE hook_action_email (
    id          serial CONSTRAINT email_key PRIMARY KEY,
    address     text,
    subject     text,
    body        text,
    hook_id     serial REFERENCES hook_trigger(id) ON DELETE CASCADE
);

-- A hook_activation represents the log of a time in which a hook was triggered.
-- It contains information about when it happened, how many clicks were
-- registered and how many minutes passed from the first relevant click to the
-- last one.
--
-- The meaning of clicks and minutes in this table is different from the ones
-- declared at the hook_trigger table. In the later, limits are described, while
-- in the former real data is logged. So it will always be true that:
--
--         hook_activation.clicks >= hook_trigger.clicks
--                           and
--        hook_activation.minutes <= hook_trigger.minutes
--
CREATE TABLE hook_activation (
    id          serial CONSTRAINT activation_key PRIMARY KEY,
    datetime    timestamp NOT NULL,
    clicks      integer,
    minutes     integer,
    hook_id     serial REFERENCES hook_trigger(id) ON DELETE CASCADE
);