# Database schema

**Master:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/database-schema/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/database-schema/commits/master)
**Devel:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/database-schema/badges/devel/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/database-schema/commits/devel)

Schema of the back end database.
